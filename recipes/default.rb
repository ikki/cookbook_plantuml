#
# Cookbook Name:: plantuml
# Recipe:: default
#
# Copyright 2014, Kazuki Seto
#
# All rights reserved - Do Not Redistribute
#

%w{ libstdc++-devel java-1.6.0-openjdk java-1.6.0-openjdk-devel }.each do |pckg|
	package "#{pckg}" do
		action :install
	end
end

bash "install graphviz" do
	cwd "/tmp"
	code <<-EOH
		wget "#{node['graphviz']['src_url']}"
		tar xf $(basename "#{node['graphviz']['src_url']}")
		cd graphviz-*
		./configure
		make
		sudo make install
	EOH
	not_if { File.exists?("/usr/local/bin/dot") }
end

bash "download plantuml" do
	cwd "/usr/local/bin"
	user "root"
	group "root"
	code <<-EOH
		wget "#{node['plantuml']['bin_url']}" -O plantuml.jar
		chmod +x plantuml.jar
	EOH
	not_if { File.exists?("/usr/local/bin/plantuml.jar") }
end
